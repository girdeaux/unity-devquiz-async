const XMLHttpRequest = require('xhr2');

const API_BASE_URL = "https://static.everyplay.com/developer-quiz/data";

async function getUsers() { return get(`${API_BASE_URL}/users`); }
async function getConversations() { return get(`${API_BASE_URL}/conversations`); }
async function getConversationMessages(id) { return get(`${API_BASE_URL}/conversations/${id}/messages`); }

const get = (uri) => {
  if (!uri) throw new Error("No URI :<");
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState == XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          const json = JSON.parse(xhr.responseText);
          resolve(json);
        } else {
          reject("Not what we expected!");
        }
      }
    };
    xhr.open('GET', uri, true);
    xhr.send(null);
  });
};

const getLatestMessageInConversation = (messages) => {
  if (messages.length === 0) return null;
  if (messages.length === 1) return messages[0];
  const sorted = messages.sort((a, b) => Date.parse(a.created_at) < Date.parse(b.created_at));
  return sorted[0];
};

async function getRecentConversationSummaries() {
  const users = await getUsers();
  const conversations = await getConversations();

  const latestMessages = [];
  for (let conv of conversations) {
    const conversationMessages = await getConversationMessages(conv.id);
    latestMessages.push(getLatestMessageInConversation(conversationMessages));
  }

  return conversations.map(conv => {
    const latestMessage = latestMessages.find(m => m.id === conv.id);

    return {
      id: conv.id,
      latest_message: {
        id: latestMessage.id,
        body: latestMessage.body,
        from_user: {
          id: latestMessage.from_user_id,
          avatar_url: users.find(user => user.id === latestMessage.from_user_id).avatar_url
        },
        created_at: latestMessage.created_at
      }
    }
  });
}

module.exports = {
  getRecentConversationSummaries: getRecentConversationSummaries,
  API_BASE_URL: API_BASE_URL,
  get: get,
  getUsers: getUsers,
  getConversations: getConversations,
  getConversationMessages: getConversationMessages,
  getLatestMessageInConversation: getLatestMessageInConversation
};
