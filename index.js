const getRecentConversationSummaries = require('./get_recent_conversation_summaries').getRecentConversationSummaries;

// Run it
(async function run() {
  console.info("Getting recent messages...");
  const summaries = await getRecentConversationSummaries();
  console.log("Conversation summaries:");
  console.log(summaries);
})();


