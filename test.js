const imports = require('./get_recent_conversation_summaries');

// Testables
const getRecentConversationSummaries = imports.getRecentConversationSummaries;
const API_BASE_URL = imports.API_BASE_URL;
const get = imports.get;
const getUsers = imports.getUsers;
const getConversations = imports.getConversations;
const getConversationMessages = imports.getConversationMessages;
const getLatestMessageInConversation = imports.getLatestMessageInConversation;

// Chai stuff
const chai = require('chai');
chai.should();
const expect = chai.expect;
const assert = chai.assert;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

// Some dummy stuff
const dummyUserJSON = [
  { "id": "1", "username": "John", "avatar_url": "http://placekitten.com/g/300/300" },
  { "id": "2", "username": "Amy", "avatar_url": "http://placekitten.com/g/301/301" },
  { "id": "3", "username": "Jeremy", "avatar_url": "http://placekitten.com/g/302/302" },
  { "id": "4", "username": "Hannah", "avatar_url": "http://placekitten.com/g/303/303" },
  { "id": "5", "username": "Charles", "avatar_url": "http://placekitten.com/g/304/304" },
  { "id": "6", "username": "George", "avatar_url": "http://placekitten.com/g/305/305" }
];

const dummyConversationsJSON = [
  { "id": "1", "with_user_id": "2", "unread_message_count": 1 },
  { "id": "2", "with_user_id": "3", "unread_message_count": 0 },
  { "id": "3", "with_user_id": "4", "unread_message_count": 0 },
  { "id": "4", "with_user_id": "5", "unread_message_count": 0 },
  { "id": "5", "with_user_id": "6", "unread_message_count": 0 }
];

const x = () => {
  return 666;
};

describe('get()', () => {
  it("gets something correctly", async function() {
    const result = await get(`${API_BASE_URL}/users`);
    result.should.deep.equal(dummyUserJSON);
  });

  it("rejects on failure", function() {
    var promise = get(`${API_BASE_URL}/notgonnawork`);
    return promise.should.be.rejected;
  });

  it("throws error if no URI supplied", function() {
    const fn = () => { get() };
    expect(fn).to.throw(Error);
  });
});

describe('getUsers()', () => {
  it('gets all users', async function() {
    const users = await getUsers();
    users.should.deep.equal(dummyUserJSON);
  });
});

describe('getConversations()', () => {
  it('gets all conversations', async function() {
    const users = await getConversations();
    users.should.deep.equal(dummyConversationsJSON);
  });
});

describe('getConversationMessages()', () => {
  it('gets all from a conversation', async function() {
    const messages = await getConversationMessages(1);
    messages.should.deep.equal([
      {
        "id": "1",
        "conversation_id": "1",
        "body": "Moi!",
        "from_user_id": "1",
        "created_at": "2016-08-25T10:15:00.670Z"
      }
    ]);
  });
});

describe('getLatestMessageInConversation()', () => {
  it('gets the latest message in a conversation', function() {
    const conv = [
      {
        "id": "1",
        "conversation_id": "1",
        "body": "Wrong",
        "from_user_id": "4",
        "created_at": "2016-08-23T10:22:00.670Z"
      },
      {
        "id": "2",
        "conversation_id": "1",
        "body": "Correct!",
        "from_user_id": "5",
        "created_at": "2016-08-23T10:23:00.670Z"
      },
      {
        "id": "3",
        "conversation_id": "1",
        "body": "Not it",
        "from_user_id": "6",
        "created_at": "2016-08-23T10:20:00.670Z"
      }
    ];

    const result = getLatestMessageInConversation(conv);
    result.should.deep.equal({
      "id": "2",
      "conversation_id": "1",
      "body": "Correct!",
      "from_user_id": "5",
      "created_at": "2016-08-23T10:23:00.670Z"
    });
  });

  it('returns null if no messages in a conversations', function() {
    const result = getLatestMessageInConversation([]);
    expect(result).to.equal(null);
  });
});

describe('getRecentConversationSummaries()', () => {
  it("should return the current user's latest conversations sorted by latest message\'s timestamp", async function() {
    const result = await getRecentConversationSummaries();
    result.should.deep.equal([
      {
        id: "1",
        latest_message: {
          id: "1",
          body: "Moi!",
          from_user: {
            id: "1",
            avatar_url: "http://placekitten.com/g/300/300",
          },
          created_at: "2016-08-25T10:15:00.670Z",
        },
      },
      {
        id: "2",
        latest_message: {
          id: "2",
          body: "Hello!",
          from_user: {
            id: "3",
            avatar_url: "http://placekitten.com/g/302/302",
          },
          created_at: "2016-08-24T10:15:00.670Z",
        },
      },
      {
        id: "3",
        latest_message: {
          id: "3",
          body: "Hi!",
          from_user: {
            id: "1",
            avatar_url: "http://placekitten.com/g/300/300",
          },
          created_at: "2016-08-23T10:15:00.670Z",
        },
      },
      {
        id: "4",
        latest_message: {
          id: "4",
          body: "Morning!",
          from_user: {
            id: "5",
            avatar_url: "http://placekitten.com/g/304/304",
          },
          created_at: "2016-08-22T10:15:00.670Z",
        },
      },
      {
        id: "5",
        latest_message: {
          id: "5",
          body: "Pleep!",
          from_user: {
            id: "6",
            avatar_url: "http://placekitten.com/g/305/305",
          },
          created_at: "2016-08-21T10:15:00.670Z",
        },
      },
    ]);
  });
});

